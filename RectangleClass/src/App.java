public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.0f,3.0f);

        // subtask 3
        // System.out.println(rectangle1);
        // System.out.println(rectangle2);   
        
        System.out.println("Dien tich hinh chu nhat 1 la " + rectangle1.getArea());
        System.out.println("Chu vi hinh chu nhat 1 la " + rectangle1.getCircumference());
        System.out.println("Dien tich hinh chu nhat 2 la " + rectangle2.getArea());
        System.out.println("Chu vi hinh chu nhat 2 la " + rectangle2.getCircumference());
    }
}