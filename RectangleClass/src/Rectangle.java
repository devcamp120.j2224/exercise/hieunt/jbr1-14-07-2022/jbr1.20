public class Rectangle {
    private float length = 1.0f;
    private float width = 1.0f;
    // khởi tạo không tham số
    public Rectangle(){
        super();
    }
    // khởi tạo đủ tham số
    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }
    // getter setter
    public float getLength() {
        return length;
    }
    public void setLength(float length) {
        this.length = length;
    }
    public float getWidth() {
        return width;
    }
    public void setWidth(float width) {
        this.width = width;
    }
    // method tính diện tích
    public float getArea() {
        return (length * width);
    }
    // method tính chu vi
    public float getCircumference() {
        return (length + width) * 2;
    }
    @Override
    public String toString() {
        return "Rectangle [length=" + length + ", width=" + width + "]";
    }
}
